package eu.wisercat.pets.rest.model;

import eu.wisercat.pets.rest.validation.UniqueCode;
import eu.wisercat.pets.rest.validation.ValidAnimal;
import eu.wisercat.pets.rest.validation.ValidColor;
import eu.wisercat.pets.rest.validation.ValidCountry;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class PetDto {

    @NotEmpty(message = "Code is required")
    @Pattern(regexp = "^[1-9][0-9]{4,}$", message = "At least 5 digits (only digits allowed)")
    @UniqueCode
    private String code;
    @Size(min = 2, message = "At least 2 characters are required")
    private String name;
    @NotEmpty(message = "Type is required")
    @ValidAnimal
    private String type;
    @NotEmpty(message = "Color is required")
    @ValidColor
    private String color;
    @NotEmpty(message = "Origin is required")
    @ValidCountry
    private String origin;
}
