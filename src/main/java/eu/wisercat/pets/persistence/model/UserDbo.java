package eu.wisercat.pets.persistence.model;

import jakarta.persistence.*;
import lombok.Data;

import java.time.LocalDateTime;
import java.util.Set;

@Data
@Entity
public class UserDbo {
    @Id
    private String username;
    private Integer hashcode;
    @OneToMany(fetch = FetchType.LAZY)
    private Set<PetDbo> pets;
    private LocalDateTime created;
    private LocalDateTime deleted;
}
