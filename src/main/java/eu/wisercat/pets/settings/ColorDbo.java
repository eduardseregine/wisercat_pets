package eu.wisercat.pets.settings;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class ColorDbo {
    @Id
    private Integer id;
    private String color;
}
