# Pets

An assessment application for wisercat.eu internship. 
Simple Angular and Spring Boot integration.

## Installation

Run 
```
./gradlew run
``` 

The application is available at http://localhost:8080

## Assignment

- User is displayed a list of pets 
- User can add pets
- A pet has a name, an identification code, a type (CAT, DOG, etc) and a fur color.
- When a new pet is saved, all mandatory fields are validated.
- All input data saved to database.
- Values of select lists are populated with data from database.
- Log in provided (with a list of the 3 user accounts) 
- Registration is not implemented.
- User is not allowed to see other user’s pets.
- User can sort pets by all columns in the table.
- User can edit pets
- A pet has a country of origin.
- Validate form fields both inline (basic) and in the back-end (comprehensive).
- Application runs with 1 command.
- Spring Boot is used.
- Embed a H2 database into the application.
- Used Liquibase as a database migration tool, 
- Executing SQL scripts on app startup.
- Used Angular for front end.
- Used Bootstrap for design and styling. 
- Multiple CSS libraries are not used.
- Mostly latest versions of all used technologies (Java 19, not 20, Spring Boot of the stable version not the latest)
- APIs are RESTful.

## TODO

- User can edit pets: backend is pending without a standard library. 
- Validation of patches is not accurate.

## Time consumed

- Started on 29 April, until May 7. Around 5.5 hours per day.

## Comprehensiveness

- Required thinking of user interface for login and sorting
- Invested extra time in backend validating with @interface
- Some time in Gradle run of an application as a single unit
- Split of frontend into components