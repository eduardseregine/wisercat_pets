package eu.wisercat.pets.persistence.service;

import eu.wisercat.pets.entity.SearchParams;
import eu.wisercat.pets.persistence.model.PetDbo;
import eu.wisercat.pets.persistence.model.UserDbo;
import eu.wisercat.pets.persistence.repository.PetRepository;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PetService {

    private final PetRepository petRepository;

    public List<PetDbo> findAllByUser(UserDbo userDbo, SearchParams searchParams) {
        Pageable params = PageRequest.of(searchParams.getFromRecord(), searchParams.getRecordCount(),
                Sort.by(searchParams.getOrderDirection(), searchParams.getOrderField().getField()));
        return petRepository.findAllByUser(userDbo, params).getContent();
    }

    public long count(UserDbo userDbo) {
        return petRepository.countAllByUser(userDbo);
    }

    public void save(PetDbo petDbo) {
        petRepository.save(petDbo);
    }

    public PetDbo findById(long code) {
        return petRepository.findById(code).orElse(null);
    }

    //TODO update pet method
}
