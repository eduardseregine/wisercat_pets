import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class ErrorService {
  public getErrorMessage(err) {
    let message = '';
    Object.keys(err).forEach((key) => {
      message += err[key] + '.  ';
    });
    return message;
  }

  public getChangeFormErrorMessages(err) {
    let message = {name: '', code: '', other: ''}
    Object.keys(err).forEach((key) => {
      if (key == 'name') {
        message.name += err[key] + '.  ';
      }
      if (key == 'code') {
        message.code += err[key] + '.  ';
      }
      if (key != 'name' && key != 'code') {
        message.other += err[key] + '.  ';
      }
    });
    return message;
  }
}
