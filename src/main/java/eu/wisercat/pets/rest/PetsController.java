package eu.wisercat.pets.rest;

import eu.wisercat.pets.component.PetFacade;
import eu.wisercat.pets.component.PetUpdateComponent;
import eu.wisercat.pets.persistence.service.SearchParamMapper;
import eu.wisercat.pets.rest.model.PatchDto;
import eu.wisercat.pets.rest.model.PetDto;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
@CrossOrigin
@Validated
@RequestMapping("/api/v1")
public class PetsController {

    private final PetFacade petFacade;
    private final SearchParamMapper searchParamMapper;
    private final PetUpdateComponent petUpdateComponent;

    @GetMapping(path = "/pets")
    public ResponseEntity searchPetsResponse(@RequestHeader(value = "Authorization") String token,
                                             @RequestParam(name = "from_record", defaultValue = "0") int fromRecord,
                                             @RequestParam(name = "record_count", defaultValue = "10") int recordsCount,
                                             @RequestParam(name = "order_direction", defaultValue = "asc") String orderDirection,
                                             @RequestParam(name = "order_field", defaultValue = "created") String orderField) {
        return petFacade.getResponse(token, searchParamMapper
                .map(fromRecord, recordsCount,
                        orderDirection, orderField));
    }

    @CrossOrigin(origins = {"*"}, maxAge = 3600, allowCredentials = "false")
    @PostMapping(path = "/pets/add", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity createPet(@RequestHeader(value = "Authorization") String token,
                                    @RequestBody @Valid PetDto petDto) {
        return petFacade.save(token, petDto);
    }

    @CrossOrigin(origins = {"*"}, maxAge = 3600, allowCredentials = "false")
    @PatchMapping(path = "/pets/edit/{code}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity updatePet(@RequestHeader(value = "Authorization") String token,
                                    @PathVariable Long code,
                                    @RequestBody List<@Valid PatchDto> patchDtoList) {
        return petUpdateComponent.getUpdateResponse(token, code, patchDtoList);
    }
}
