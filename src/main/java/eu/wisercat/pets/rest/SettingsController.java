package eu.wisercat.pets.rest;

import eu.wisercat.pets.persistence.service.SettingsFactory;
import eu.wisercat.pets.rest.model.SettingsDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@AllArgsConstructor
@CrossOrigin
@RequestMapping("/api/v1")
public class SettingsController {

    private final SettingsFactory settingsFactory;

    @GetMapping(path = "/settings")
    public ResponseEntity<SettingsDto> getSettings() {
        return ResponseEntity.ok(settingsFactory.generateRecentSettings());
    }
}
