package eu.wisercat.pets.mapper;

import eu.wisercat.pets.entity.Pet;
import eu.wisercat.pets.persistence.model.PetDbo;
import eu.wisercat.pets.settings.AnimalDbo;
import eu.wisercat.pets.settings.AnimalDboMapper;
import eu.wisercat.pets.settings.ColorDboMapper;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PetDboMapper {

    @Resource
    private UserDboMapper userDboMapper;
    @Resource
    private ColorDboMapper colorDboMapper;
    @Resource
    private AnimalDboMapper animalDboMapper;

    public PetDbo map(Pet pet) {
        if (pet == null) {
            return null;
        }
        PetDbo dbo = new PetDbo();
        dbo.setCode(pet.getCode());
        dbo.setUser(userDboMapper.map(pet.getUser()));
        dbo.setColorDbo(colorDboMapper.map(pet.getColor()));
        dbo.setType(animalDboMapper.map(pet.getAnimal()));
        dbo.setName(pet.getName());
        dbo.setOrigin(pet.getOrigin());
        dbo.setCreated(pet.getCreated());
        dbo.setDeleted(pet.getDeleted());
        return dbo;
    }

    public Pet map(PetDbo dbo) {
        if (dbo == null) {
            return null;
        }
        Pet pet = new Pet();
        pet.setCode(dbo.getCode());
        pet.setUser(userDboMapper.map(dbo.getUser()));
        pet.setAnimal(animalDboMapper.map(dbo.getType()));
        pet.setColor(colorDboMapper.map(dbo.getColorDbo()));
        pet.setName(dbo.getName());
        pet.setOrigin(dbo.getOrigin());
        pet.setCreated(dbo.getCreated());
        pet.setDeleted(dbo.getDeleted());
        return pet;
    }

    public List<Pet> map(List<PetDbo> dbos) {
        return dbos.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
