package eu.wisercat.pets.mapper;

import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.rest.model.UserDto;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class UserDtoMapper {

    public User map(String accessHeader) {
        User user = new User();
        String username = getUsername(accessHeader);
        user.setUsername(username);
        user.setHashcode(getPassword(accessHeader, username).hashCode());
        user.setCreated(LocalDateTime.now());
        return user;
    }

    public User map(UserDto userDto) {
        return map(userDto.getToken().getToken());
    }

    private String getUsername(String accessHeader) {
        return accessHeader.split("_")[0].trim();
    }

    private String getPassword(String accessHeader, String username) {
        return accessHeader.length() > username.length() + 1 ? accessHeader.substring(username.length() + 1) : "";
    }
}
