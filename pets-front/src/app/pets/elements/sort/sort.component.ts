import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.css']
})
export class SortComponent {
  @Input()
  fieldName: string;
  @Input()
  orderBy: string;
  @Input()
  direction: string;
  @Input()
  onClick: (field: string) => void;

  private titles = new Map<string, string>([['name', 'Name'], ["code","Code"],
    ['type', 'Type'], ["color","Fur color"], ["origin", "Country"]]);

  private sorts = ['bi bi-filter', 'bi bi-sort-down-alt', 'bi bi-sort-up'];

  getSort() {
    return this.fieldName != this.orderBy ? this.sorts[0] : this.direction == "asc" ? this.sorts[1] : this.sorts[2];
  }

  getTitle() {
    return this.titles.get(this.fieldName);
  }
}
