package eu.wisercat.pets.rest.validation;

import eu.wisercat.pets.settings.SettingsService;
import jakarta.annotation.Resource;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

class ValidAnimalValidator implements ConstraintValidator<ValidAnimal, String> {

    @Resource
    private SettingsService settingsService;

    @Override
    public void initialize(ValidAnimal constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return settingsService.getAllAnimals().stream()
                .anyMatch(x -> x.getAnimal().equals(value));
    }
}