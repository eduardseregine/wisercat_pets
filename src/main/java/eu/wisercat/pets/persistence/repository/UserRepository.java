package eu.wisercat.pets.persistence.repository;

import eu.wisercat.pets.persistence.model.UserDbo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserDbo, String> {
}
