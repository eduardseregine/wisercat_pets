package eu.wisercat.pets.persistence.service;

import eu.wisercat.pets.persistence.model.UserDbo;
import eu.wisercat.pets.persistence.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@AllArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Optional<UserDbo> findUserByUsername(String username) {
        return userRepository.findById(username);
    }
}
