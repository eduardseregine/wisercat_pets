package eu.wisercat.pets.persistence.service;

import eu.wisercat.pets.rest.model.SettingsDto;
import eu.wisercat.pets.settings.Animal;
import eu.wisercat.pets.settings.Color;
import eu.wisercat.pets.settings.SettingsService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class SettingsFactory {

    private static SettingsDto settingsDto;
    private final SettingsService settingsService;

    public SettingsDto generateRecentSettings() {
        if (settingsDto == null) {
            prepare();
        }
        return settingsDto;
    }

    private void prepare() {
        settingsDto = new SettingsDto();
        settingsDto.setTypes(mapAnimals(settingsService.getAllAnimals()));
        settingsDto.setColors(mapColors(settingsService.getAllColors()));
    }

    private List<String> mapAnimals(List<Animal> animals) {
        return animals.stream().map(x -> x.getAnimal()).collect(Collectors.toList());
    }

    private List<String> mapColors(List<Color> colors) {
        return colors.stream().map(x -> x.getColor()).collect(Collectors.toList());
    }
}
