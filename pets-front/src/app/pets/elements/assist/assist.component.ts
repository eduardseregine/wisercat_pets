import { Component } from '@angular/core';

@Component({
  selector: 'app-assist',
  templateUrl: './assist.component.html',
  styleUrls: ['./assist.component.css']
})
export class AssistComponent {
    text = 'Users with (3), (2) and (0) pets: firstUser, secondUser, thirdUser';
    text2 = 'Password is always "testPassword123"';
}
