package eu.wisercat.pets.persistence.repository;

import eu.wisercat.pets.persistence.model.PetDbo;
import eu.wisercat.pets.persistence.model.UserDbo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PetRepository extends JpaRepository<PetDbo, Long> {
    Page<PetDbo> findAllByUser(UserDbo userDbo, Pageable params);

    Long countAllByUser(UserDbo userDbo);

    //TODO Set pet field query
}
