package eu.wisercat.pets.rest.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Arrays;
import java.util.Locale;

class ValidCountryValidator implements ConstraintValidator<ValidCountry, String> {

    @Override
    public void initialize(ValidCountry constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return Arrays.stream(Locale.getISOCountries())
                .map(country -> Locale.of("en", country).getCountry())
                .anyMatch(countryCode -> countryCode.equals(value));
    }
}