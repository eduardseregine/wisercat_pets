package eu.wisercat.pets.persistence.model;

import eu.wisercat.pets.settings.AnimalDbo;
import eu.wisercat.pets.settings.ColorDbo;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import java.time.LocalDateTime;

@Data
@Entity
public class PetDbo {
    @Id
    private Long code;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @Fetch(FetchMode.JOIN)
    private UserDbo user;
    private String name;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @Fetch(FetchMode.JOIN)
    private AnimalDbo type;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(nullable = false)
    @Fetch(FetchMode.JOIN)
    private ColorDbo colorDbo;
    private String origin;
    private LocalDateTime created;
    private LocalDateTime deleted;
}
