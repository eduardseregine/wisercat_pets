import {Component, Input} from '@angular/core';
import {LoginService} from "../../services/login.service";

@Component({
  selector: 'app-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.css']
})
export class AvatarComponent {

  constructor(private loginService: LoginService) {
  }

  public getUsername() {
    return this.loginService.username;
  }
}
