package eu.wisercat.pets.entity;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;

@Data
public class User {
    private String username;
    private int hashcode;
    private Set<Pet> pets;
    private LocalDateTime created;
    private LocalDateTime deleted;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return hashcode == user.hashcode && username.equals(user.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, hashcode, created, deleted);
    }
}