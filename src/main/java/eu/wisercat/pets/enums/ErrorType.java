package eu.wisercat.pets.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@AllArgsConstructor
public enum ErrorType {
    Unauthorized("{\"authorization\": \"username doesn't exist or password is incorrect\"}", HttpStatus.UNAUTHORIZED),
    IncorrectOrderField("{\"order_field\": \"field does not exist\"}", HttpStatus.BAD_REQUEST),
    IncorrectPath("{\"code\": \"object not found\"}", HttpStatus.NOT_FOUND),
    Unprocessable("{\"patch\": \"unprocessable\"}", HttpStatus.UNPROCESSABLE_ENTITY),
    NotSupported("{\"op\": \"not supported type of modification\"}", HttpStatus.METHOD_NOT_ALLOWED);

    @Getter
    private String message;
    private HttpStatus status;

    public ResponseEntity getResponse() {
        return ResponseEntity
                .status(status)
                .body(message);
    }
}
