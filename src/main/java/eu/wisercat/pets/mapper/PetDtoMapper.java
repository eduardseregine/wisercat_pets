package eu.wisercat.pets.mapper;

import eu.wisercat.pets.entity.Pet;
import eu.wisercat.pets.rest.model.PetDto;
import eu.wisercat.pets.settings.SettingsService;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PetDtoMapper {

    @Resource
    private SettingsService settingsService;

    public Pet map(PetDto dto) {
        Pet pet = new Pet();
        pet.setCode(Long.parseLong(dto.getCode()));
        pet.setName(dto.getName());
        pet.setColor(settingsService.getFirstByColor(dto.getColor()));
        pet.setAnimal(settingsService.getFirstByAnimal(dto.getType()));
        pet.setOrigin(dto.getOrigin());
        pet.setCreated(LocalDateTime.now());
        return pet;
    }

    public PetDto map(Pet pet) {
        PetDto dto = new PetDto();
        dto.setCode(String.valueOf(pet.getCode()));
        dto.setName(pet.getName());
        dto.setType(pet.getAnimal().getAnimal());
        dto.setColor(pet.getColor().getColor());
        dto.setOrigin(pet.getOrigin());
        return dto;
    }

    public List<PetDto> map(List<Pet> pets) {
        return pets.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
