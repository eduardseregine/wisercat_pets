import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../services/login.service';
import {IUser} from "../../model/IUser";
import {Router} from '@angular/router';
import {ErrorService} from '../../services/error.service';
import { FormGroup, FormControl , Validators} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {
  protected linkOnClose = '/pets';
  protected errorText = '';
  protected loginForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  constructor(private loginService: LoginService, private router: Router,
              private errorService: ErrorService) {
  }

  public onClick() {
    const user: IUser = this.getUser();
    this.loginService.createToken(user).subscribe({
      next: (data) => {
        this.router.navigateByUrl("/pets").then(() => this.loginService.saveToken(data.token));
      }, error: (error) => {
        this.errorText = this.errorService.getErrorMessage(error.error);
      }
    });
  }

  get title() {
    return this.loginService.isTokenSaved() ? 'Change User': 'Login';
  }

  get closeVisibility() {
    return this.loginService.isTokenSaved() ? 'visible': 'invisible';
  }

  private getUser(): IUser {
    return {username: this.loginForm.get('username').value,
      password: this.loginForm.get('password').value};
  }
}
