package eu.wisercat.pets.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum OrderField {
    code("code"),
    name("name"),
    type("typeId"),
    color("colorDboId"),
    origin("origin"),
    created("created");

    @Getter
    String field;
}
