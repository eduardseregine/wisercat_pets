import {Component, Input, OnInit} from '@angular/core';
import {PetService} from "../../services/pet.service";
import {SettingsService} from "../../services/settings.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ErrorService} from "../../services/error.service";
import {IPet} from "../../model/IPet";

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Input()
  public initialPet: IPet = {code: 0, name: '', type: '', color: '', origin: ''};
  types = [];
  colors = [];
  countries = ['EE', 'LV', 'LT', 'FI', 'SE', 'NO'];
  errorMessages = {name: '', code: '', other: ''};

  constructor(private settingsService: SettingsService, private petService: PetService,
              private router: Router, private errorService: ErrorService) {
    const navigation = this.router.getCurrentNavigation();
    const state: IPet = navigation.extras.state as {
      name: string,
      code: number,
      type: string,
      color: string,
      origin: string
    };
    if (state != undefined) {
      this.initialPet = state;
    }
  }

  ngOnInit(): void {
    this.settingsService.getSettings().subscribe(data => {
      this.types = data.types;
      this.colors = data.colors;
    });
    this.updateForm();
  }

  protected changeForm = new FormGroup({
    name: new FormControl(this.initialPet.name, Validators.required),
    code: new FormControl(this.initialPet.code, Validators.pattern('^[1-9][0-9][0-9]*$')),
    type: new FormControl(this.initialPet.type),
    color: new FormControl(this.initialPet.color),
    origin: new FormControl(this.initialPet.origin)
  });

  public onClick() {
    const pet: IPet = this.getPet();
    this.petService.changePet(pet, this.initialPet).subscribe({
      next: (data) => {
        this.router.navigateByUrl("/pets").then(() => {
        });
      }, error: (error) => {
        this.errorMessages = this.errorService.getChangeFormErrorMessages(error.error);
      }
    });
  }

  private updateForm() {
    if (this.initialPet.code != 0) {
      this.changeForm.setValue({
        name: this.initialPet.name,
        code: this.initialPet.code,
        type: this.initialPet.type,
        color: this.initialPet.color,
        origin: this.initialPet.origin
      });
      this.changeForm.controls['code'].disable();
    }
  }

  private getPet(): IPet {
    return {
      code: this.changeForm.get('code').value, name: this.changeForm.get('name').value,
      type: this.changeForm.get('type').value, color: this.changeForm.get('color').value,
      origin: this.changeForm.get('origin').value
    };
  }

  get title() {
    return this.initialPet.code === 0 ? 'Add a new pet' : 'Edit a pet';
  }

  get buttonTitle() {
    return this.initialPet.code === 0 ? 'Add pet' : 'Edit pet';
  }
}
