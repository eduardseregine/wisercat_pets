import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {IPet} from "../model/IPet";
import {IPatch} from "../model/IPatch";

@Injectable({
    providedIn: 'root'
})

export class PetService {
    url = 'http://localhost:8080/api/v1/';


    constructor(private http: HttpClient) {
    }

    getPets(orderField: string, orderDirection: string): Observable<any> {
        return this.http.get(this.url + 'pets?order_field=' + orderField + '&order_direction=' + orderDirection,
            this.requestOptions);
    }

    changePet(pet: IPet, petOld: IPet): Observable<any> {
        const code = pet.code;
        if (petOld.code !== code) {
            return this.createPet(pet);
        }
        return this.editPet(code, this.getPatches(pet, petOld));
    }

    createPet(pet: IPet): Observable<any> {
        return this.http.post(this.url + 'pets/add', pet, this.requestOptions);
    }

    editPet(code: number, patches: IPatch[]): Observable<any> {
        return this.http.patch(this.url + 'pets/edit/' + code, patches, this.requestOptions);
    }

    get requestOptions() {
        return {headers: new HttpHeaders({Authorization: localStorage.getItem("token")})};
    }

    getPatches(pet: IPet, petOld: IPet): IPatch[] {
        let patches = [];
        for (const field in pet) {
            if (pet[field] != petOld[field]) {
                patches.push({op:'update', path:'/' + field, value: pet[field]});
            }
        }
        return patches;
    }
}
