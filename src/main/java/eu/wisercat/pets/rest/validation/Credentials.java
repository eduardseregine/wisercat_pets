package eu.wisercat.pets.rest.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({ElementType.TYPE})
@Retention(RUNTIME)
@Constraint(validatedBy = CredentialsValidator.class)
@Documented
public @interface Credentials {

    String message() default "User does not exist or password is incorrect";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
