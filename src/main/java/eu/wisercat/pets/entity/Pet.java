package eu.wisercat.pets.entity;

import eu.wisercat.pets.settings.Animal;
import eu.wisercat.pets.settings.Color;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class Pet {
    private Long code;
    private User user;
    private String name;
    private Animal animal;
    private Color color;
    private String origin;
    private LocalDateTime created;
    private LocalDateTime deleted;
}
