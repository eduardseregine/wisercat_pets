package eu.wisercat.pets.settings;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ColorDboMapper {
    public Color map(ColorDbo dbo) {
        if (dbo == null) {
            return null;
        }
        Color color = new Color();
        color.setId(dbo.getId());
        color.setColor(dbo.getColor());
        return color;
    }

    public ColorDbo map(Color color) {
        if (color == null) {
            return null;
        }
        ColorDbo dbo = new ColorDbo();
        dbo.setId(color.getId());
        dbo.setColor(color.getColor());
        return dbo;
    }

    public List<Color> map(List<ColorDbo> dbos) {
        return dbos.stream().map(this::map).collect(Collectors.toList());
    }
}
