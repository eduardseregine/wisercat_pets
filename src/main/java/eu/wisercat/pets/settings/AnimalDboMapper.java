package eu.wisercat.pets.settings;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AnimalDboMapper {
    public Animal map(AnimalDbo dbo) {
        if (dbo == null) {
            return null;
        }
        Animal animal = new Animal();
        animal.setId(dbo.getId());
        animal.setAnimal(dbo.getAnimal());
        return animal;
    }

    public AnimalDbo map(Animal animal) {
        if (animal == null) {
            return null;
        }
        AnimalDbo dbo = new AnimalDbo();
        dbo.setId(animal.getId());
        dbo.setAnimal(animal.getAnimal());
        return dbo;
    }

    public List<Animal> map(List<AnimalDbo> dbos) {
        return dbos.stream().map(this::map).collect(Collectors.toList());
    }
}
