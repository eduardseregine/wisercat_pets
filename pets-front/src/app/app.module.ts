
import {forwardRef, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NG_VALUE_ACCESSOR, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import {RouterOutlet} from "@angular/router";
import { ListComponent } from './pets/views/list/list.component';
import {HttpClientModule} from "@angular/common/http";
import { SortComponent } from './pets/elements/sort/sort.component';
import { FormComponent } from './pets/views/form/form.component';
import { AvatarComponent } from './pets/elements/avatar/avatar.component';
import { CloseComponent } from './pets/elements/close/close.component';
import { HeaderComponent } from './pets/elements/header/header.component';
import { FooterComponent } from './pets/elements/footer/footer.component';
import { TextFieldComponent } from './pets/elements/text.field/text.field.component';
import { SelectFieldComponent } from './pets/elements/select.field/select.field.component';
import { LoginComponent } from './pets/views/login/login.component';
import { AssistComponent } from './pets/elements/assist/assist.component';


@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    SortComponent,
    FormComponent,
    AvatarComponent,
    CloseComponent,
    HeaderComponent,
    FooterComponent,
    TextFieldComponent,
    SelectFieldComponent,
    LoginComponent,
    AssistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterOutlet,
    NgbModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => TextFieldComponent),
      multi: true,
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
