package eu.wisercat.pets.component;

import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.mapper.UserDboMapper;
import eu.wisercat.pets.mapper.UserDtoMapper;
import eu.wisercat.pets.persistence.model.UserDbo;
import eu.wisercat.pets.persistence.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@AllArgsConstructor
public class UserComponent {

    private final UserService userService;
    private final UserDboMapper userDboMapper;
    private final UserDtoMapper userDtoMapper;


    public User getAuthorizedUser(String accessHeader) {
        return userDboMapper.map(getAuthorizedUser(userDtoMapper.map(accessHeader)));
    }

    public UserDbo getAuthorizedUser(User user) {
        Optional<UserDbo> optionalUserDbo = userService.findUserByUsername(user.getUsername());
        if (optionalUserDbo.isPresent()) {
            User authorized = userDboMapper.map(optionalUserDbo.get());
            if (authorized.equals(user)) {
                return optionalUserDbo.get();
            }
        }
        return null;
    }
}
