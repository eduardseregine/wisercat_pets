package eu.wisercat.pets.rest.model;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class PatchDto {
    @Pattern(regexp = "^update$", message = "Only update allowed")
    String op;
    @Pattern(regexp = "^/(?:name|type|color|origin)$", message = "Not allowed field name")
    String path;
    @NotEmpty(message = "Value cannot be empty")
    @Size(min = 2, message = "At least 2 characters are required")
    String value;

    public String getPath() {
        if (path.startsWith("/")) {
            return path.substring(1);
        }
        return path;
    }
}
