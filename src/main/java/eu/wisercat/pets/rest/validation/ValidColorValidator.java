package eu.wisercat.pets.rest.validation;

import eu.wisercat.pets.settings.SettingsService;
import jakarta.annotation.Resource;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

class ValidColorValidator implements ConstraintValidator<ValidColor, String> {

    @Resource
    private SettingsService settingsService;

    @Override
    public void initialize(ValidColor constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return settingsService.getAllColors().stream()
                .anyMatch(x -> x.getColor().equals(value));
    }
}