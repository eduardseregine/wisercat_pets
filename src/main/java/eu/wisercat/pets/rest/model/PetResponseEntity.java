package eu.wisercat.pets.rest.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class PetResponseEntity {
    private List<PetDto> pets;
    private long total;

    public PetResponseEntity(List<PetDto> pets) {
        this.pets = pets;
        this.total = pets.size();
    }
}
