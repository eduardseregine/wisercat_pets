package eu.wisercat.pets.rest.validation;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = ValidColorValidator.class)
@Documented
public @interface ValidColor {

    String message() default "Incorrect color name";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
