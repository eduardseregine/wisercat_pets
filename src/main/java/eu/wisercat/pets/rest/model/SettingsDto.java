package eu.wisercat.pets.rest.model;

import lombok.Data;

import java.util.List;

@Data
public class SettingsDto {
    private List<String> types;
    private List<String> colors;
}
