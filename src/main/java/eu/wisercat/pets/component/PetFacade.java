package eu.wisercat.pets.component;

import eu.wisercat.pets.entity.SearchParams;
import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.enums.ErrorType;
import eu.wisercat.pets.rest.model.PetDto;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PetFacade {
    private final UserComponent userComponent;
    private final PetComponent petComponent;

    public ResponseEntity getResponse(String accessHeader, SearchParams searchParams) {
        User user = userComponent.getAuthorizedUser(accessHeader);
        if (user == null) {
            return ErrorType.Unauthorized.getResponse();
        }
        if (searchParams == null) {
            return ErrorType.IncorrectOrderField.getResponse();
        }
        return petComponent.findAll(user, searchParams);
    }

    public ResponseEntity save(String accessHeader, PetDto petDto) {
        User user = userComponent.getAuthorizedUser(accessHeader);
        if (user == null) {
            return ErrorType.Unauthorized.getResponse();
        }
        return petComponent.save(user, petDto);
    }
}
