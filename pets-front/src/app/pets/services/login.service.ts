import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {IUser} from "../model/IUser";

@Injectable({
  providedIn: 'root'
})

export class LoginService {
  url = 'http://localhost:8080/api/v1/user';

  constructor(private http: HttpClient) { }

  createToken(user: IUser): Observable<any> {
    return this.http.post(this.url, user);
  }

  saveToken(token: string) {
    localStorage.setItem('token', token);
  }

  isTokenSaved() {
    return this.token != undefined && this.token.length > 0;
  }

  get token() {
    return localStorage.getItem('token');
  };

  get username() {
    return this.isTokenSaved() ? "@" + this.token.split("_")[0] : 'login';
  }
}
