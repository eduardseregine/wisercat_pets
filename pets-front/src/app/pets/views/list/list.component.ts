import {Component, OnInit} from '@angular/core';
import {PetService} from '../../services/pet.service';
import {IPet} from "../../model/IPet";
import {NavigationExtras, Router} from "@angular/router";

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public pets: IPet[] = [];
  public orderBy = 'created';
  public orderDirection = 'asc';
  private regionNames = new Intl.DisplayNames(['en'], {type: 'region'});

  constructor(private petService: PetService, private router: Router) {
  }

  ngOnInit(): void {
    this.updatePetsByField('created');
  }

  public onClickFunction = (fieldName: string): void => {
    if (this.orderBy == fieldName) {
      this.revertOrderDirection();
    } else {
      this.orderDirection = 'asc';
    }
    this.orderBy = fieldName;
    this.updatePetsByField(fieldName);
  }

  public getCountry(code: string) {
    return this.regionNames.of(code);
  }

  private updatePetsByField(fieldName: string) {
    this.petService.getPets(fieldName, this.orderDirection).subscribe(data => {
      this.pets = data.pets;
    });
  }

  private revertOrderDirection(): void {
    if (this.orderDirection == 'asc') {
      this.orderDirection = 'desc';
    } else {
      this.orderDirection = 'asc';
    }
  }

  protected directToEdit(pet: IPet) {
    const navigationExtras: NavigationExtras = {
      state: {
        code: pet.code,
        name: pet.name,
        type: pet.type,
        color: pet.color,
        origin: pet.origin
      }
    };
    this.router.navigate(['/edit'], navigationExtras).then(() =>{});
   }
}
