package eu.wisercat.pets.rest.validation;

import eu.wisercat.pets.component.UserComponent;
import eu.wisercat.pets.mapper.UserDtoMapper;
import eu.wisercat.pets.rest.model.UserDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
class CredentialsValidator implements ConstraintValidator<Credentials, UserDto> {

    private final UserComponent userComponent;
    private final UserDtoMapper userDtoMapper;

    @Override
    public void initialize(Credentials constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(UserDto userDto, ConstraintValidatorContext context) {
        return !userDto.isEmpty() && userComponent.getAuthorizedUser(userDtoMapper.map(userDto)) != null;
    }
}