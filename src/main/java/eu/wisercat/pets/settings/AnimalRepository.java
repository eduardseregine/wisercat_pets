package eu.wisercat.pets.settings;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalDbo, Integer> {
    AnimalDbo getFirstByAnimal(String animal);
}
