package eu.wisercat.pets.settings;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class SettingsService {

    private final AnimalRepository animalRepository;
    private final ColorRepository colorRepository;
    private final AnimalDboMapper animalDboMapper;
    private final ColorDboMapper colorDboMapper;

    public List<Animal> getAllAnimals() {
        return animalDboMapper.map(animalRepository.findAll());
    }

    public List<Color> getAllColors() {
        return colorDboMapper.map(colorRepository.findAll());
    }

    public Animal getFirstByAnimal(String animal) {
        return animalDboMapper.map(animalRepository.getFirstByAnimal(animal));
    }

    public Color getFirstByColor(String color) {
        return colorDboMapper.map(colorRepository.getFirstByColor(color));
    }
}
