package eu.wisercat.pets.mapper;

import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.persistence.model.UserDbo;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UserDboMapper {


    public User map(UserDbo dbo) {
        if (dbo == null) {
            return null;
        }
        User user = new User();
        user.setUsername(dbo.getUsername());
        user.setHashcode(dbo.getHashcode());
        user.setCreated(dbo.getCreated());
        user.setDeleted(dbo.getDeleted());
        return user;
    }

    public UserDbo map(User user) {
        if (user == null) {
            return null;
        }
        UserDbo dbo = new UserDbo();
        dbo.setUsername(user.getUsername());
        dbo.setHashcode(user.getHashcode());
        dbo.setCreated(user.getCreated());
        dbo.setDeleted(user.getDeleted());
        return dbo;
    }
}
