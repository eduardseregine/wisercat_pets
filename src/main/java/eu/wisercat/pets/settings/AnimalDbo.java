package eu.wisercat.pets.settings;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class AnimalDbo {
    @Id
    private Integer id;
    private String animal;
}
