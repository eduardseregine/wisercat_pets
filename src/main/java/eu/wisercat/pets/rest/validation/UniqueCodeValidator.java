package eu.wisercat.pets.rest.validation;

import eu.wisercat.pets.persistence.service.PetService;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.regex.Pattern;

@Service
@AllArgsConstructor
class UniqueCodeValidator implements ConstraintValidator<UniqueCode, String> {

    private final PetService petService;

    @Override
    public void initialize(UniqueCode constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (!Pattern.compile("[0-9]*").matcher(value).matches()) {
            return true;
        }
        return petService.findById(Long.parseLong(value)) == null;
    }
}