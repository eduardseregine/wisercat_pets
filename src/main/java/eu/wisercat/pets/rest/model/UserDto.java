package eu.wisercat.pets.rest.model;

import eu.wisercat.pets.rest.validation.Credentials;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Credentials
public class UserDto {

    @NotEmpty
    private String username;
    @NotEmpty
    private String password;

    public Token getToken() {
        return new Token(username + "_" + password);
    }

    public boolean isEmpty() {
        return username.isEmpty() || password.isEmpty();
    }
}
