import {Component, Input} from '@angular/core';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-text-field',
  templateUrl: './text.field.component.html',
  styleUrls: ['./text.field.component.css']
})
export class TextFieldComponent {
  @Input()
  name = 'Field name';
  @Input()
  errorText = '';
  @Input()
  control: FormControl;

  getErrorClass() {
    return this.errorText.length > 0 ? 'visible' : 'invisible';
  }

  getFrontendErrorMessage() {
    return this.name != 'Code' ? "Input is required" : 'Digits are required';
  }
}
