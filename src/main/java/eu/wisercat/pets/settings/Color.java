package eu.wisercat.pets.settings;

import lombok.Data;

@Data
public class Color {
    private int id;
    private String color;
}
