import {Component, Input} from '@angular/core';
import {FormControl} from "@angular/forms";

@Component({
  selector: 'app-select-field',
  templateUrl: './select.field.component.html',
  styleUrls: ['./select.field.component.css']
})
export class SelectFieldComponent {
  @Input()
  name = 'Field name';
  @Input()
  errorText = '';
  @Input()
  values = ['One', 'Two', 'Three'];
  @Input()
  control: FormControl;
  regionNames = new Intl.DisplayNames(['en'], {type: 'region'});

  getOption(value: string) {
    return this.name == 'Country' ? this.regionNames.of(value) : value;
  }

  getErrorClass() {
    return this.errorText.length > 0 ? 'visible' : 'invisible';
  }

  isSelected(value: string): boolean {
    return this.control.value == value;
  }

  get isChooseSelected() {
    return this.values.filter(value => this.control.value == value).length == 0;
  }
}
