package eu.wisercat.pets.component;

import eu.wisercat.pets.entity.SearchParams;
import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.mapper.PetDboMapper;
import eu.wisercat.pets.mapper.PetDtoMapper;
import eu.wisercat.pets.mapper.UserDboMapper;
import eu.wisercat.pets.persistence.model.PetDbo;
import eu.wisercat.pets.persistence.model.UserDbo;
import eu.wisercat.pets.persistence.service.PetService;
import eu.wisercat.pets.rest.model.PetDto;
import eu.wisercat.pets.rest.model.PetResponseEntity;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class PetComponent {

    private final PetService petService;
    private final PetDboMapper petDboMapper;
    private final PetDtoMapper petDtoMapper;
    private final UserDboMapper userDboMapper;

    public ResponseEntity<PetResponseEntity> findAll(User user, SearchParams searchParams) {
        PetResponseEntity entity = new PetResponseEntity();
        UserDbo userDbo = userDboMapper.map(user);
        entity.setPets(petDtoMapper.map(petDboMapper.map(petService.findAllByUser(userDbo, searchParams))));
        entity.setTotal(petService.count(userDbo));
        return ResponseEntity.ok(entity);
    }

    public ResponseEntity save(User user, PetDto petDto) {
        UserDbo userDbo = userDboMapper.map(user);
        PetDbo petDbo = petDboMapper.map(petDtoMapper.map(petDto));
        petDbo.setUser(userDbo);
        petService.save(petDbo);
        return ResponseEntity.status(201).build();
    }
}
