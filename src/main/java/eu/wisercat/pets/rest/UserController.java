package eu.wisercat.pets.rest;

import eu.wisercat.pets.rest.model.Token;
import eu.wisercat.pets.rest.model.UserDto;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@AllArgsConstructor
@RequestMapping("/api/v1")
@CrossOrigin
public class UserController {

    @CrossOrigin(origins = {"*"}, maxAge = 3600, allowCredentials = "false")
    @PostMapping(path = "/user", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Token> createToken(@RequestBody @Valid UserDto userDto) {
        return ResponseEntity.ok(userDto.getToken());
    }
}
