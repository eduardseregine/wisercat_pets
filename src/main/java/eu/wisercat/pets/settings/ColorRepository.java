package eu.wisercat.pets.settings;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ColorRepository extends JpaRepository<ColorDbo, Integer> {
    ColorDbo getFirstByColor(String color);
}
