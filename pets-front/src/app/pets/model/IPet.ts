export interface IPet {
  code: number;
  name: string;
  type: string;
  color: string;
  origin: string;
}
