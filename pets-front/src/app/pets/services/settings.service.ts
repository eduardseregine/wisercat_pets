import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class SettingsService {
  url = 'http://localhost:8080/api/v1/settings';

  constructor(private http: HttpClient) { }

  getSettings(): Observable<any> {
    return this.http.get(this.url);
  }
}
