package eu.wisercat.pets.persistence.service;

import eu.wisercat.pets.entity.SearchParams;
import eu.wisercat.pets.enums.OrderField;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Arrays;

@Service
public class SearchParamMapper {

    public SearchParams map(int fromRecord, int recordsCount,
                            String orderDirection, String orderFieldString) {
        if (!orderDirection.equalsIgnoreCase("desc")) {
            orderDirection = "asc";
        }
        return isValidOrderField(orderFieldString) ? new SearchParams(Math.max(0, fromRecord),
                Math.min(1000, Math.max(1, recordsCount)),
                Sort.Direction.valueOf(orderDirection.toUpperCase()),
                OrderField.valueOf(orderFieldString.toLowerCase())) : null;
    }

    private boolean isValidOrderField(String orderFieldString) {
        return Arrays.stream(OrderField.values()).anyMatch(x -> x.name()
                .equalsIgnoreCase(orderFieldString));
    }
}
