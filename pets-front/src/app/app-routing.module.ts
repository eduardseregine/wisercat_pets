import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ListComponent} from "./pets/views/list/list.component";
import {FormComponent} from "./pets/views/form/form.component";
import {LoginComponent} from "./pets/views/login/login.component";

const routes: Routes = [
  { path: 'add', component: FormComponent },
  { path: 'edit', component: FormComponent },
  { path: 'pets', component: ListComponent},
  { path: '**', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {  })],
  exports: [RouterModule],
})
export class AppRoutingModule { }
