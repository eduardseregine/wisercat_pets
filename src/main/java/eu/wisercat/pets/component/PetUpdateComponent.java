package eu.wisercat.pets.component;

import eu.wisercat.pets.entity.Pet;
import eu.wisercat.pets.entity.User;
import eu.wisercat.pets.enums.ErrorType;
import eu.wisercat.pets.mapper.PetDboMapper;
import eu.wisercat.pets.mapper.PetDtoMapper;
import eu.wisercat.pets.persistence.service.PetService;
import eu.wisercat.pets.rest.model.PatchDto;
import eu.wisercat.pets.settings.SettingsService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class PetUpdateComponent {

    private final PetService petService;
    private final PetDboMapper petDboMapper;
    private final PetDtoMapper petDtoMapper;
    private final UserComponent userComponent;
    private final SettingsService settingsService;

    public ResponseEntity getUpdateResponse(String token, Long code, List<PatchDto> patchDtoList) {
        User user = userComponent.getAuthorizedUser(token);
        if (user == null) {
            return ErrorType.Unauthorized.getResponse();
        }
        Pet pet = petDboMapper.map(petService.findById(code));
        if (pet == null) {
            return ErrorType.IncorrectPath.getResponse();
        }
        if (patchDtoList.stream().anyMatch(x -> !x.getOp().equals("update"))) {
            return ErrorType.NotSupported.getResponse();
        }
        if (patchDtoList.size() == 0 ||
                patchDtoList.stream().map(x -> x.getPath()).distinct().count() < patchDtoList.size()) {
            return ErrorType.Unprocessable.getResponse();
        }
        if (!patchDtoList.stream().allMatch(x -> isUpdated(pet, x))) {
            return ErrorType.Unprocessable.getResponse();
        }
        petService.save(petDboMapper.map(pet));
        return ResponseEntity.ok(petDtoMapper.map(pet));
    }

    private boolean isUpdated(Pet pet, PatchDto patchDto) {
        String value = patchDto.getValue();
        switch (patchDto.getPath()) {
            case "name":
                String oldName = pet.getName();
                pet.setName(value);
                return isCorrectValue(oldName, value);
            case "type":
                String oldAnimal = pet.getAnimal().getAnimal();
                pet.setAnimal(settingsService.getFirstByAnimal(value));
                return settingsService.getFirstByAnimal(value) != null &&
                        isCorrectValue(oldAnimal,
                                settingsService.getFirstByAnimal(value).getAnimal());
            case "color":
                String oldColor = pet.getColor().getColor();
                pet.setColor(settingsService.getFirstByColor(value));
                return settingsService.getFirstByColor(value) != null &&
                        isCorrectValue(oldColor,
                                settingsService.getFirstByColor(value).getColor());
            case "origin":
                String oldOrigin = pet.getOrigin();
                pet.setOrigin(value);
                return isCorrectValue(oldOrigin, value);
            default: return false;
        }
    }

    private boolean isCorrectValue(String initialValue, String valueToSet) {
        return valueToSet != null && !initialValue.equals(valueToSet);
    }
}

