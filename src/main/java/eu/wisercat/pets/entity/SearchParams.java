package eu.wisercat.pets.entity;

import eu.wisercat.pets.enums.OrderField;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.domain.Sort;
@Data
@AllArgsConstructor
public class SearchParams {
    private int fromRecord;
    private int recordCount;
    private Sort.Direction orderDirection;
    private OrderField orderField;
}
