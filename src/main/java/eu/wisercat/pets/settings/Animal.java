package eu.wisercat.pets.settings;

import lombok.Data;

@Data
public class Animal {
    private int id;
    private String animal;
}
